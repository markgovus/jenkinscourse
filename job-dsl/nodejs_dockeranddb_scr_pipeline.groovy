pipelineJob('nodejs_dockeranddb_scr_pipeline') {
  definition {
    cpsScm {
      scm {
        git {
          remote {
            credentials('gitlab')
            url('https://gitlab.com/markgovus/nodejsdockerdemo.git')
            name('gitlab')
          }
          branch('*/master')
        }
      }
      scriptPath('jenkinsfiles/Jenkinsfilev2')
      lightweight(true)
    }
  }
}