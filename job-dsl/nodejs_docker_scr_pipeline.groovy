pipelineJob('nodejs_docker_scr_pipeline') {
  definition {
    cpsScm {
      scm {
        git {
          remote {
            credentials('gitlab')
            url('https://gitlab.com/markgovus/nodejsdockerdemo.git')
            name('gitlab')
          }
          branch('*/master')
        }
      }
      scriptPath('jenkinsfiles/Jenkinsfile')
      lightweight(true)
    }
  }
}