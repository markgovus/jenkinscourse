job('course_nodejs_docker_example') {
    disabled(true)
    scm {
        git('https://gitlab.com/markgovus/nodejsdockerdemo.git') {  node -> // is hudson.plugins.git.GitSCM
            node / gitConfigName('DSL User')
            node / gitConfigEmail('mgovus@mountainblueitsolutions.co.uk')
        }
    }
    triggers {
        scm('H/5 * * * *')
    }
    wrappers {
        nodejs('nodejs') // this is the name of the NodeJS installation in 
                         // Manage Jenkins -> Configure Tools -> NodeJS Installations -> Name
    }
    steps {
        dockerBuildAndPublish {
            repositoryName('markgovus/docker-nodejs-demo')
            tag('${GIT_REVISION,length=9}')
            dockerHostURI('tcp://docker.for.win.localhost:2375')
            registryCredentials('dockerreg')
            forcePull(false)
            forceTag(false)
            createFingerprints(false)
            skipDecorate()
            skipTagAsLatest()
        }
    }
}
