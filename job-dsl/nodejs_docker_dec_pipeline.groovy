pipelineJob('nodejs_docker_dec_pipeline') {
    disabled(true)
    definition {
        cpsScm {
            scm {
                git {
                  remote {
                    credentials('gitlab')
                    url('https://gitlab.com/markgovus/nodejsdockerdemo.git')
                    name('gitlab')
                  }
                  branch('*/master')
                }
            }
            scriptPath('jenkinsfiles/Jenkinsfile.dec')
            lightweight(true)
        }
    }
}