job('seed_project') {
    scm {
        git('https://gitlab.com/markgovus/jenkinscourse.git') {  node -> // is hudson.plugins.git.GitSCM
            node / gitConfigName('DSL User')
            node / gitConfigEmail('mgovus@mountainblueitsolutions.co.uk')
        }
    }
    triggers {
            scm('H/5 * * * *')
    }
    steps {
        dsl {
            external('job-dsl/nodejs.groovy')
            external('job-dsl/nodejs_docker.groovy')
            external('job-dsl/nodejs_docker_scr_pipeline.groovy')
            external('job-dsl/nodejs_dockeranddb_scr_pipeline.groovy')
            external('job-dsl/nodejs_dockerandsq_scr_pipeline.groovy')
            removeAction('DISABLE')
        }
    }
}